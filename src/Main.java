import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;


public class Main {
    private static ChromeDriver page;
    private static WebElement home;

    private static String baseUrl = "http://localhost/diyt/app/";
    public static void main(String [] args) throws InterruptedException {


        System.setProperty("webdriver.chrome.driver", "./chromedriver-2");
        page = new ChromeDriver();
        login();

        tryHtmlCode();
        WebElement startLearning;
        WebElement textarea;
        WebElement run;

        /*
        * write on css
        * */

        tryCssCode("css.php", "tutorc.php", "" +
                "<h1 style=\"color:red;\">Testing</h1>\n" +
                "<br/>\n" +
                "<h2 style=\"color:green;\">header2</h2>");

        /*
        * java script
        * */

        tryCssCode("js.php", "tutorj.php", "<script>alert(\"dsds\");</script>");

        if(isAlertPresent()){
           // page.switchTo().alert().accept();
        }


        tryCssCode("php.php", "tutorp.php", "<?php echo(\"hellophp\") ?>");


        textarea = tryPhpCode("php.php", "tutorp.php", "<?php echo(\"hellophp\") ?>");


        textarea.sendKeys("<?php echo \"nahom\" echo \"dsds\" ?>");
        page.manage().timeouts().implicitlyWait(1,TimeUnit.SECONDS);
        run =  ((ChromeDriver) page).findElementById("runn");
        run.click();

        /*
        * php code
        * */

        tryCssCode("python.php", "tutorpy.php", "print \"hello\"");

        takeTest();


    }

    private static void takeTest() {
        page.navigate().to(baseUrl+"html.php");
        page.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);

        page.navigate().to(baseUrl+"htmltests.php");
        page.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);

        WebElement navmenu =  page.findElementByXPath("/html/body/header/nav/div/div[1]/span");
        navmenu.click();

        WebElement htmlbtn =  page.findElementByXPath("//*[@id=\"mySidenav\"]/button");
        htmlbtn.click();

        WebElement runn = page.findElementByXPath("/html/body/div/form/button");
        runn.click();
    }

    private static WebElement tryPhpCode(String s, String s2, String s3) {
        WebElement startLearning;
        WebElement textarea;
        WebElement run;
        page.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

        page.navigate().to(baseUrl + s);
        page.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        startLearning = ((ChromeDriver) page).findElementByXPath("/html/body/div[2]/div/div[2]/button");
//        startLearning.click();

        page.navigate().to(baseUrl + s2);
        page.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        textarea = ((ChromeDriver) page).findElementById("eTextarea");

        textarea.sendKeys(s3);
        page.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        run = ((ChromeDriver) page).findElementById("runn");
        run.click();
        return textarea;
    }

    private static void tryCssCode(String s, String s2, String s3) {
        WebElement startLearning;
        WebElement run;
        WebElement textarea = tryPhpCode(s, s2, s3);
    }

    private static void tryHtmlCode() {
        page.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);

        page.navigate().to(baseUrl+"html.php");
        page.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);

        WebElement startLearning =  ((ChromeDriver) page).findElementByXPath("/html/body/div[2]/div/div[2]/button");
//        startLearning.click();

        page.navigate().to(baseUrl+"tutorh.php");
        page.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);

        WebElement textarea =  ((ChromeDriver) page).findElementById("eTextarea");

        textarea.sendKeys("<h1>Testing</h1><br/><h2>header2</h2>");
        page.manage().timeouts().implicitlyWait(1,TimeUnit.SECONDS);
        WebElement run =  ((ChromeDriver) page).findElementById("runn");
        run.click();
    }

    private static void login() {
        page.get(baseUrl+"index.php");
        page.manage().window().fullscreen();
        WebElement input = ((ChromeDriver) page).findElementById("login");
        input.click();

        page.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);

        WebElement inputUser =  ((ChromeDriver) page).findElementById("logusername");
        inputUser.sendKeys("nahom");

        WebElement inputPassword =  ((ChromeDriver) page).findElementById("logpassword");
        inputPassword.sendKeys("nahom123");

        WebElement loginbtn =  ((ChromeDriver) page).findElementById("loginbtn");
        loginbtn.click();
    }

    public static boolean isAlertPresent() {

        boolean presentFlag = false;

        try {

            // Check the presence of alert
            Alert alert = page.switchTo().alert();
            // Alert present; set the flag
            presentFlag = true;
            // if present consume the alert
            alert.accept();
            //( Now, click on ok or cancel button )

        } catch (NoAlertPresentException ex) {
            // Alert not present
            ex.printStackTrace();
        }

        return presentFlag;
    }


}